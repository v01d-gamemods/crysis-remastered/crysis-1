from trainerbase.codeinjection import CodeInjection
from trainerbase.memory import Address, pm


freeze_ammo = CodeInjection(
    pm.base_address + 0x3B61B2,
    b"\x90" * 4,
)

freeze_energy = CodeInjection(
    pm.base_address + 0x4F971E,
    b"\x90" * 6,
)

freeze_health = CodeInjection(
    pm.base_address + 0x69C1BF,
    b"\x90" * 8,
)
